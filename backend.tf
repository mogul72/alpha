
  /*
  you can use an S3 bucket for holding terraform.tfstate files
  or a local file - i'll give access to the S3 bucket .
  Although it's public  i can limit access to readonly on the file as you need
  to know the whole path via an IP ingres ACL..

  https://s3-eu-west-1.amazonaws.com/bh16-gbuk-14/projects/terraform/alpha
  */

  // using my test bucket in Ireland.
  terraform {
    backend "s3" {
      bucket = "bh16-gbuk-14"
      key    = "projects/terraform/alpha/terraform.tfstate"
      region = "eu-west-1"
    }
  }
