
/*

using cloud-init data (to install MQTT via ansbile and tweak firewall config)
*/

# Render a part using a `template_file`

data "template_file" "script" {
  template = "${file("${path.module}/user_data")}"

}

# Render a multi-part cloud-init config making use of the part
# above, and other source files - copied from the web..

data "template_cloudinit_config" "config" {
  gzip = true
  base64_encode = true

  # Main cloud-config configuration file.
  part {
    filename = "user_data"
    content_type = "text/cloud-config"
    content = "${data.template_file.script.rendered}"
  }

}