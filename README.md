# README #

Readme for  terraform code to support .


### What is this repository for? ###

* Quick summary

Example of how to use terraform code and create the following

* EC2 instance using Ubuntu 18.04 LTS configured with a VPC Subnet
* Mosquitto MTQQ IoT Broker service configured to listen on tcp/port 8883 
* User Data to provide apt install commands to install MQTT and change default port from 1883 to 8883
- tested the default install of mqtt via manual config and cloud-init  - not quite worked properly using c-init

# Tested

* ec2 instance creation and security grp


# Left to do

* comment out the public 8883 port for MQTT in the code
* create NLB for the instance with a public IP, apply the security group to allow it pulic access
* Use a launch configuraton to apply the correct config on starting up of a new instance within the ASH so all instances are correctly configured
 - Assumed that the instance initially created is going to be added into the ELB pool group.
* didn't get time to create the Auto scaling group in terraform - and create the CloudWatch monitoring
 * create a cloudwatch alert trigger to do alarm  using Terraform
 
 
