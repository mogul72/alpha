output "private_ip"
{
  description = "IP address of EC2 instance"
  value = ["${aws_instance.IoT-1.private_ip}"]

}

output "public_hostname" {
  description = "Public hostname of EC2 instance"
  value = [ "${aws_instance.IoT-1.public_dns}" ]
}