// Defined variables from BASH variables pulled from ~/.aws/credentials

variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  // Set region to ireland
  default     = "eu-west-2"
}


variable aws_key_name {
  default     = "aws"

}

// use this pubkey key in AWS to gain ssh privs / run ansible against
resource "aws_key_pair" "deployer" {
  key_name = "aws"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZToUI3n11KRhKrOnQiYizmSZD9wvsh6xPiA1t5LWQIeBKLCq4C3pJ1xgCTsdzL9pBcV5TlIPDnuPVPu8hQ6DbaLDty0QwxRQE9xsDdgC2fn+bTkYMaOOtLaSxqzpLKR8wSt6NiVksqpkqo+pk8u0AILc8yDmH7bK3cK3BTxwcsQrU/4KG1JoIyjDIRjNMxNg7Hoc4Cy7J9MMbfFkrLw1EDfB3lWwkw3tS5G0WIyc8sezkgUxYpIEmI5HLXySTRO9PRDgQM4VwOav0r+3d1+MLa7mGY42CE9CpfRkpPmluhtw8qZ9YC5iQPUpgL+q8rbpnr/k1jQHUF5i7BXFb/cnx daniel@Daniels-MBP.kcnet"

}

  # Define  AWS AMI per region for London for multi region deployment
variable "amis" {
  description = "AMIs by region"
  default = {
    eu-west-1 = "ami-0f24d681ed9df89f3"
    eu-west-2 = "ami-0fb09aa40ff851e45"
  }
}

