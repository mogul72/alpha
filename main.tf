
/*
Allow access for our instance via TCP port 8883 from
 the internet

*/
resource "aws_security_group" "iot" {
  name = "vpc_iot"
  description = "Allow incoming connections for Mosquito MQTT broker/server"

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
  }

  ingress {
    from_port = 8883
    to_port = 8883
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  // linux vm admin from local IP and will comment out post implementation

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "IoTSG"
  }
}


resource "aws_instance" "IoT-1" {
  ami = "${lookup(var.amis, var.aws_region)}"
  availability_zone = "eu-west-2a"
  instance_type = "t2.micro"
  key_name = "${var.aws_key_name}"
  vpc_security_group_ids = ["${aws_security_group.iot.id}"]

  //subnet_id = "${aws_subnet.eu-west-2a-public.id}"

  associate_public_ip_address = true
  source_dest_check = false
  user_data_base64 = "${data.template_cloudinit_config.config.rendered}"

//
//  # Start an AWS instance with the cloud-init config as user data
//  resource "aws_instance" "web" {
//    ami              = "ami-d05e75b8"
//    instance_type    = "t2.micro"
//    user_data_base64 = "${data.template_cloudinit_config.config.rendered}"
//  }

  tags {
    Name = "IoT"
  }
}

